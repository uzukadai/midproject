from django.shortcuts import render

# Create your views here.

def born (request):
    return render(request, 'born/born.html')

def skills (request):
    return render(request, 'skills/skills.html')

def school (request):
    return render(request, 'school/school.html')

def experiences (request):
    return render(request, 'experiences/experiences.html')

def contact (request):
    return render(request, 'contact/contact.html')