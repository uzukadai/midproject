from django.contrib import admin
from django.conf.urls import url, include

from . import views
from myapp import views as myappViews

urlpatterns = [
    url('admin/', admin.site.urls),
    url(r'^$', views.home),
    url(r'^born/', myappViews.born),
    url(r'^school/', myappViews.school),
    url(r'^experiences/', myappViews.experiences),
    url(r'^skills/', myappViews.skills),
    url(r'^contact/', myappViews.contact),
]
